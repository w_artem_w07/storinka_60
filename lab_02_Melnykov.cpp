#include <iostream>
#include <math.h>
#include <cmath>
using namespace std;

int main()
{
    double x;
    int n;
    double result = 0.0;

    cout << "Enter the value of x: ";
    cin >> x;

    cout << "Enter the number of iterations: ";
    cin >> n;

    for (int i = 0; i <= n; i++)
    {
        double numerator = pow(-1, i + 1) * sin(2 * i + 1) * x;
        double denominator = pow(2 * i + 1, 2);

        result += numerator / denominator;
    }

    cout << "The result of the equation is: " << result << endl;

    return 0;
}

double x() {
    double pi = 3.14159265358979323846;
    double a = 0.0; // ��������� �������� ���������
    double b = pi / 2.0; // ������ �������� ���������
    double x = (a + b) / 2.0; // �������� ���������
    double eps = 1e-6; // �������� ����������

    // ����������� ������
    while (std::abs(pi * x / 4.0) > eps && std::abs(b - a) > eps) {
        if (pi * x / 4.0 > 0) {
            b = x;
        }
        else {
            a = x;
        }
        x = (a + b) / 2.0;
    }
    return 0;
}


